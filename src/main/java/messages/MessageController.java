package messages;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;

@Controller
public class MessageController {
    private ArrayList<Entry> entries = new ArrayList<>();

    @GetMapping("/messages")
    public String printMessages(@ModelAttribute Entry entry, Model model) {
        model.addAttribute("entries", entries);
        return "messages.html";
    }

    @PostMapping("/messages")
    public String addMessage(@ModelAttribute Entry entry, Model model) {
        if (!entry.getAuthor().isEmpty() && !entry.getMessage().isEmpty()) {
            entries.add(entry);
        }
        return printMessages(entry, model);
    }
}