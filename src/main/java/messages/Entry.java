package messages;

public class Entry {
    private String author;
    private String message;

    public Entry(String author, String message) {
        this.author = author;
        this.message = message;
    }

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }
}
