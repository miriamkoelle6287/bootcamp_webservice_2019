# Exercise 06

### Formular
Wir wollen nun ein Formular ergänzen um neue Nachrichten zu verfassen und auszugeben.

a) messages.html: Ergänze das folgende HTML-Formular:
```
<form action="#" th:action="@{/messages}" th:object="${entry}">
    <p><input type="text" th:field="*{author}" placeholder="Name"/></p>
    <p><input type="text" th:field="*{message}" placeholder="Message"/></p>
    <p><input type="submit" value="Submit"/></p>
</form> 
```

b) messages.html: Ergänze die Formular-Methode "post"

c) MessageController: Passe den Controller an (bitte erstmal so hinnehmen:) )  
indem du einen zweiten Parameter übergibst:  
```
public String printMessages(@ModelAttribute Entry entry, Model model) { 
    ...
}
```

d) MessageController: Schreibe einen zweiten Controller-Endpunkt "addMessage"
```
    public String addMessage(@ModelAttribute Entry entry, Model model) {
        return printMessages(entry, model);
    }
```

e) MessageController: Ergänze die Annotation für ein PostMapping (Pfad "messages")

f) MessageController: Wann immer der Controller aufgerufen wird, soll der übergebene Entry zur Messages-Liste hinzugefügt werden.

g) Was bedeutet PostMapping und was ist der Zusammenhang zwischen Controller und Formular?

h) Bonus: Wieso muss @ModelAttribute Entry entry in Aufgabe c) gesetzt werden? Tipp: Schau dir auch das Formular genau an.