# Exercise 03

### MessageController - CSS
 
a) Lege die folgende Ordner-Struktur an:  src/main/resources/static  
und lege ein CSS-File "styles.css" im static-Folder an.

b) definiere eine blaue Schriftfarbe für Überschriften 1. Ordnung (h1)

c) referenziere das CSS-File im Head-Bereich von messages.html. Hierfür reicht einfach nur der File-Name "styles.css" ohne Pfad.
